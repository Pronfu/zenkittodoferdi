# Zenkit To Do for Ferdi / Franz
This is a [Ferdi](https://github.com/getferdi/ferdi) / [Franz](https://github.com/meetfranz/franz) recipe/plugin for This is a Ferdi / Franz recipe/plugin for [ZenKit To Do](https://todo.zenkit.com) which is owned by [Zenkit](https://zenkit.com/).

## Installation
1. Open the Franz Plugins (`dev`elopment) folder on your machine, if the directory does not exist, create it:
  * Mac: `~/Library/Application Support/Ferdi/recipes/dev/`
    * `mkdir ~/Library/Application Support/Ferdi/recipes/dev/`
  * Linux: `~/.config/Ferdi/recipes/dev/`
    * `mkdir ~/.config/Ferdi/recipes/dev/
  * Windows: `%appdata%/Ferdi/recipes/dev/`
    
2. Download or clone this repo into the `dev` directory – as sub-directory `ZenKitToDoFerdi` (e.g.).
3. Reload Ferdi.
4. Add New Service.
5. Click on Custom Services.
6. Click on Element (under Community 3rd Party Recipes).
7. Go to the Element service and login.

## Huge thanks to
[Sylvain Cecchetto](https://github.com/sy6sy2) (for their [Element for Franz](https://github.com/sy6sy2/recipe-riot)) for the basis of this.
[Simon Lüke for their fork of Element for Franz](https://github.com/semaphor/recipe-element) which I found on Google.
[Zenkit](https://zenkit.com/) for having an online version of their [Zenkit To Do](https://zenkit.com/en/todo/).

## License

[MIT](https://choosealicense.com/licenses/mit/)