// What is required in order to integrate a service
module.exports = (Franz) => {
  // check for new messages every second and update Franz badge
  Franz.loop(getMessages)

  // Not sure how to setup notifications for this, as there isn't a specific div that has the alerts
};

// Kept simple on purpose, if you wanted to add or change it then read https://meetfranz.com/developer/recipe/frontend for details. 